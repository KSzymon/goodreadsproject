package good.main;

public class BookDetails {
	
	//Used to give each book a unique id 
	private static int book_id;
	
	//Book detais 
	private String bookName; 
	private int PageCount; 
	private Author author;
	private int id; 
	
	//Used to calculate book rating 
	private double rating; 
	private int numberOfRatings; // Raing == total points / numberof user that rated a book
	private double totalRating ; 
	
	public BookDetails(String bookName, int pageCount, Author author) {
		this.bookName = bookName;
		PageCount = pageCount;
		this.author = author;
		this.id = ++book_id;
	}
	
	
	public double giveRating(double rating) {
			totalRating += rating;		
			rating = totalRating / ++numberOfRatings;
		return rating; } //Current Rating  
	
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}
	
	public int getID() {
		return this.id;
	}

	public static int getUniqueIDCount() {
		return book_id;
	}
}
