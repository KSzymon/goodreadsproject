package good.main;

public class Author {
	
	private static int unique_id;
	private int id;
	
	private String name; 
	private int age; //replace by date
	
	
	public Author(String name, int age) {
		this.name = name;
		this.age = age;
		this.id = ++unique_id;
	}
	public Author() {
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public static int getUnique_id() {
		return unique_id;
	}
	public int getId() {
		return id;
	}

}
