package good.main;

import java.util.HashMap;

public class Main {

	public static void main(String[] args) {
		
		console c = new console();
		c.setUp();
	
		
		c.findBookByAuthorName("Martin");
		
		System.out.println("=============\n");
		
		c.searchBookByID(10);
		
		
		System.out.println("=============\n");
		c.searchBookByName("Winds of Winter");
	}

}
