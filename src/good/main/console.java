package good.main;

import java.awt.print.Book;
import java.util.ArrayList;
import java.util.HashMap;

public class console {
	
	//Key = uniqueID Value = book/author object
	
	
	private static HashMap<Integer, BookDetails> booksMap = new HashMap<Integer, BookDetails>();
	private static HashMap<Integer, Author> AuthorMap = new HashMap<Integer, Author>();
	
	
	public void runConsole() {

	}
	
	public void setUp() {
		
		AuthorMap.put(Author.getUnique_id() +1, new Author("Sanderson", 56));
		AuthorMap.put(Author.getUnique_id() +1, new Author("Martin", 44));
	
		
		booksMap.put(BookDetails.getUniqueIDCount() +1, new  BookDetails("Winds of Winter", 777, addAuthorToBook("Sanderson")));
		booksMap.put(BookDetails.getUniqueIDCount() +1, new  BookDetails("A dream of a Spring", 4456, addAuthorToBook("Martin")));
	
	}
	
	public ArrayList<String> findBookByAuthorName(String name) {
				ArrayList<String> bookList = new ArrayList<>();
					
				for(BookDetails b: booksMap.values()) {
						if(b.getAuthor().getName().equals(name));
							bookList.add(b.getBookName());
							System.out.println(b.getBookName());
				}
				
		return bookList;
	}
	
	public void searchBookByName(String bookname) {

			for(BookDetails b : booksMap.values()) {
				if(b.getBookName().equals(bookname)) {
					System.out.println(bookname + " found in storage");
					break;
				} else { 
					System.out.println(bookname + " has not beed found ");
				}
			}
	}

	public void searchBookByID(int id) {
		
		if(booksMap.get(id) != null) {
			BookDetails b = booksMap.get(id);
			System.out.println(b.getBookName());
		} else { 
			System.out.println("Book not recognised");
		}
	}

	public Author addAuthorToBook(String name) {
		
		Author temp = new Author();
		for(Author a : AuthorMap.values()) {
			if(a.getName().equals(name)) {
				temp = a;
			}
		}
		return temp;
	}

}
